---
name: Epic
about: Template for creating epics
title: "[EPIC] my epic title"
labels: 'type: epic'
assignees: ''

---

## Owner ##

## Description and Context ##

## Stories ##

- [ ]  Story one
- [ ]  Story two
- [ ]  Story three
