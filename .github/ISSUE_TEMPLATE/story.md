---
name: Story
about: Describe this issue template's purpose here.
title: As a [type of user], I want [goal] so that I [receive benefit].
labels: 'type: story'
assignees: ''

---

## Description and Context ##

## Acceptance criteria ##

## Dependencies ##

Is blocked by:

- [ ]  Story one
- [ ]  Task two
- [ ]  Epic three
